package com.laschinger.keybricks;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GamePanel extends JPanel {

	private HashMap<Color, BufferedImage> _tileIcons;
	private BufferedImage _background;
	private BufferedImage _letters;
	private HashSet<Tile> _tiles = new HashSet<Tile>();
	
	public GamePanel(HashMap<Color, BufferedImage> _tileIcons2, BufferedImage background, BufferedImage letters) {
		_tileIcons = _tileIcons2;
		_background = background;
		_letters = letters;
	}
	
	public void paintComponents(Graphics g){
		g.drawImage(_background, 0, 0, null);
		
		int w = getWidth()/KeyBricks.getSize()[0];
		int h = getHeight()/KeyBricks.getSize()[1];
		int useSize = 0;
		
		if (w > h) {
			useSize = h;
		} else {
			useSize = w;
		}
		
		if (!_tiles.isEmpty()){
			for (Tile tile: _tiles){
				g.drawImage(_tileIcons.get(tile.getColor()), tile.getXPos()*useSize, tile.getYPos()*useSize, null);
				g.drawImage(getLetterImage(tile.getLetter()), tile.getXPos()*useSize, tile.getYPos()*useSize, null);
			}
		}
		
	}
	
	private BufferedImage getLetterImage(char letter){
		letter = Character.toLowerCase(letter);
		char[] letters = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
		
		int index = 0;
		int i = 0;
		
		for (char l: letters){
			if (l == letter){
				index = i;
			}
			i++;
		}
		
		BufferedImage subImage = _letters.getSubimage((_letters.getWidth()/26)*index, 0, _letters.getHeight(), _letters.getHeight());
		
		return subImage;
	}
	
	public void updateGraphics(HashSet<Tile> gameTiles) {
		
		_tiles = gameTiles;
		
		for (int j = KeyBricks.getSize()[0] - 1; j >= 0; j--){
			for (int i = KeyBricks.getSize()[1] - 1; i >= 0; i--){
				
				boolean empty = true;
				
				for (Tile tile: _tiles){
					if(tile.getXPos() == j && tile.getYPos() == i){
						empty = false;
					}
				}
				
				if (empty){
					boolean filled = false;
					int k = i;
						
					while (!filled && k >= 0) {
						for (Tile testTile: _tiles){
							if (testTile.hasPos(j, k)){
								testTile.setYPos(i);
								filled = true;
							}
						}
						k--;
					}
				}
			}
		}
		
		paintComponents(getGraphics());
	}

	public void setGameTiles(HashSet<Tile> tiles) {
		_tiles = tiles;
	}

}
