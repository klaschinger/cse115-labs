package com.laschinger.keybricks;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class KeyBricks implements Runnable{
	
	private static BufferedImage _gameIcon;
	private static BufferedImage _settingsIcon;
	private static BufferedImage _logo;
	private static HashMap<Color,BufferedImage> _tileIcons = new HashMap<Color, BufferedImage>();
	private static BufferedImage _background;
	private static BufferedImage _letters;
	private static int[] _size = {1,4};
	private static int _colorAmount = 4;

	@Override
	public void run() {
		LoadImages();
		
		JFrame frame = new JFrame("KeyBricks");
		JPanel mainPanel = new JPanel();
		JPanel controlPanel = new JPanel();
		GamePanel gamePanel = new GamePanel(_tileIcons, _background, _letters);
		IconPanel iconPanel = new IconPanel("logo.png");
		JLabel newGameText = new JLabel("New Game");
		JButton newGame = new JButton("Start");
		JLabel scoreText = new JLabel("Score");
		JLabel score = new JLabel("0");
		JLabel singlesText = new JLabel("Single Blocks");
		JPanel singles = new JPanel();
		JLabel settingsText = new JLabel("Settings");
		JButton settings = new JButton();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setMinimumSize(new Dimension(800,600));
		frame.setResizable(false);
		frame.setIconImage(_gameIcon);
		frame.addComponentListener(new ResizeListener(frame));
		frame.add(mainPanel);
		
		mainPanel.setLayout(null);
		mainPanel.add(controlPanel);
		mainPanel.add(gamePanel);
		
		controlPanel.setLayout(null);
		controlPanel.setSize(frame.getWidth()/4, frame.getHeight());
		controlPanel.setLocation(0,0);
		controlPanel.add(newGameText);
		controlPanel.add(newGame);
		controlPanel.add(scoreText);
		controlPanel.add(score);
		controlPanel.add(singlesText);
		controlPanel.add(singles);
		controlPanel.add(settingsText);
		controlPanel.add(settings);
		
		gamePanel.setSize((frame.getWidth()/4)*3 - 20, frame.getHeight()-20);
		gamePanel.setLocation(frame.getWidth()/4, 0);
		
		iconPanel.setSize(controlPanel.getWidth()-20, 100);
		iconPanel.setLocation(10, 10);
		iconPanel.paintComponents(iconPanel.getGraphics());
		
		newGame.setFont(new Font("Times New Roman", Font.PLAIN, controlPanel.getWidth()/10));
		newGame.setSize(newGame.getPreferredSize());
		newGame.setLocation(controlPanel.getWidth()/2 - newGame.getWidth()/2, iconPanel.getHeight() + 20);
		newGame.addActionListener(new StartGameListener(gamePanel, newGame, _gameIcon));
		
		settings.setFont(new Font("Times New Roman", Font.PLAIN, controlPanel.getWidth()/10));
		settings.setSize(newGame.getPreferredSize());
		settings.setLocation(controlPanel.getWidth()/2 - settings.getWidth()/2, controlPanel.getHeight() - 100);
		settings.setIcon(new ImageIcon(_settingsIcon.getScaledInstance(settings.getHeight(), settings.getHeight(), Image.SCALE_SMOOTH)));
		settings.addActionListener(new SettingsListener(_gameIcon));
		
		frame.pack();
		frame.setVisible(true);
	}
	
	private static void LoadImages(){
		
		try{
			_gameIcon = ImageIO.read(KeyBricks.class.getClassLoader().getResource("icon.png"));
			_settingsIcon = ImageIO.read(KeyBricks.class.getClassLoader().getResource("settings_icon.png"));
			//_logo = ImageIO.read(KeyBricks.class.getClassLoader().getResource("logo.png"));
			_tileIcons.put(Color.RED, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_red.png")));
			_tileIcons.put(Color.BLUE, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_blue.png")));
			_tileIcons.put(Color.GREEN, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_green.png")));
			_tileIcons.put(Color.YELLOW, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_yellow.png")));
			_tileIcons.put(Color.ORANGE, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_orange.png")));
			_tileIcons.put(Color.MAGENTA, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_magenta.png")));
			_tileIcons.put(Color.PINK, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_pink.png")));
			_tileIcons.put(Color.CYAN, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_cyan.png")));
			_tileIcons.put(Color.GRAY, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_gray.png")));
			_tileIcons.put(Color.DARK_GRAY, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_dark_gray.png")));
			_background = ImageIO.read(KeyBricks.class.getClassLoader().getResource("background.png"));
			_letters = ImageIO.read(KeyBricks.class.getClassLoader().getResource("letters.png"));
		} catch (IOException e){
			e.printStackTrace();
		} catch (NullPointerException e ) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
	
	public static int[] getSize(){
		return _size;
	}
	
	public static void setSize(int[] i){
		_size = i;
	}

	public static int getColors() {
		return _colorAmount;
	}
	
	public static void setColors(int colors){
		_colorAmount = colors;
	}

}
