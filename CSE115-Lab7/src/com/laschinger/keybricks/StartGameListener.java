package com.laschinger.keybricks;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.JButton;

public class StartGameListener implements ActionListener {

	private GamePanel _gamePanel;
	private JButton _button;
	private Image _icon;
	
	public StartGameListener(GamePanel panel, JButton button, Image icon){
		_gamePanel = panel;
		_button = button;
		_icon = icon;
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		JButton button = (JButton) e.getSource();
		button.setEnabled(false);
		
		Game game = new Game(KeyBricks.getSize()[0], KeyBricks.getSize()[1], _gamePanel, _button, _icon);
		for (KeyListener l: _gamePanel.getKeyListeners()){
			_gamePanel.removeKeyListener(l);
		}
		
		_gamePanel.addKeyListener(game);
		_gamePanel.setGameTiles(game.getTiles());
		_gamePanel.paintComponents(_gamePanel.getGraphics());
		_gamePanel.requestFocus();

	}

}
