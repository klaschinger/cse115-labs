package com.laschinger.keybricks;

import javax.swing.SwingUtilities;

public class Driver {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new KeyBricks());
	}

}