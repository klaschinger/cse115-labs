package com.laschinger.keybricks;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

public class ButtonChangeListener implements ActionListener {

	ButtonGroup _group;
	
	public ButtonChangeListener(ButtonGroup g){
		_group = g;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButton){
			JRadioButton button = (JRadioButton) e.getSource();
			int[] i = {0,0};
			
			_group.clearSelection();
			
			if(button.getText().equals("Easy")){
				i[0] = 1;
				i[1] = 4;
			} else if (button.getText().equals("Medium")) {
				i[0] = 4;
				i[1] = 4;
			} else if (button.getText().equals("Hard")) {
				i[0] = 8;
				i[1] = 8;
			}
			
			KeyBricks.setSize(i);
			button.setSelected(true);
		}

	}

}
