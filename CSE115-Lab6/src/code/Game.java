package code;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class Game implements Runnable{
	
	@Override
	public void run() {
		JFrame frame = new JFrame("Sequence Game");
		JButton start = new JButton("<html><font face=\"verdana\" size=\"12\">Start</font></html>");
		JLabel label = new JLabel("<html><p><font face=\"verdana\" size=\"10\">Instructions</font></p>"
				+ "<p><font face=\"verdana\" size=\"4\">Click the \"start\" button to start the game</font></p>"
				+ "<p><font face=\"verdana\" size=\"4\">Click as many of the red buttons as you can before time runs out!</font></p></html>"
				);
		Timer timer = new Timer(10000, new GameTimer());
		
		label.setHorizontalAlignment(SwingConstants.CENTER);
		
		timer.setRepeats(false);
		
		start.addActionListener(new Start(timer, frame));
		
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(),BoxLayout.Y_AXIS));
		frame.getContentPane().add(start);
		
		frame.add(label);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
}
