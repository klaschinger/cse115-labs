package code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Button implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		Start.chooseButton();
		Start.incrementScore();
	}

}
