package code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;

public class Start implements ActionListener {

	private Timer _timer;
	private JFrame _frame;
	private static JButton _buttonA;
	private static JButton _buttonB;
	private static JButton _buttonC;
	
	static int _score = 0;
	
	public Start(Timer timer, JFrame frame){
		_timer = timer;
		_frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		_timer.start();
		
		_frame.getContentPane().removeAll();
		_frame.getContentPane().setLayout(new BoxLayout(_frame.getContentPane(),BoxLayout.X_AXIS));
		
		_buttonA = new JButton("<html><font size=\"16\">A</font></html>");
		_buttonA.addActionListener(new Button());
		_frame.getContentPane().add(_buttonA);
		
		_buttonB = new JButton("<html><font size=\"16\">B</font></html>");
		_buttonB.addActionListener(new Button());
		_frame.getContentPane().add(_buttonB);
		
		_buttonC = new JButton("<html><font size=\"16\">C</font></html>");
		_buttonC.addActionListener(new Button());
		_frame.getContentPane().add(_buttonC);
		
		_frame.pack();
		
		chooseButton();
	}

	public static void chooseButton() {
		
		Random rand = new Random();
		int number = rand.nextInt(3);
		
		switch(number){
			case 0:
				_buttonA.setEnabled(true);
				_buttonB.setEnabled(false);
				_buttonC.setEnabled(false);
				break;
			case 1: 
				_buttonA.setEnabled(false);
				_buttonB.setEnabled(true);
				_buttonC.setEnabled(false);
				break;
			case 2: 
				_buttonA.setEnabled(false);
				_buttonB.setEnabled(false);
				_buttonC.setEnabled(true);
				break;
			default:
				System.out.println("Error");
		}
	}

	public static void incrementScore() {
		_score++;
	}
	
	public static String getClicks() {
		return String.valueOf(_score);
	}

}
