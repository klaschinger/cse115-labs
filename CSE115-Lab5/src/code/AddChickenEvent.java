package code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import example1.BarnYard;
import example1.Chicken;

public class AddChickenEvent implements ActionListener {
	
	BarnYard _barnyard;
	
	public AddChickenEvent(BarnYard by){
		_barnyard = by;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Chicken c = new Chicken();
		
		_barnyard.addChicken(c);
		
		JButton start = new JButton("Start");
		start.addActionListener(new StartChicken(c));
		_barnyard.addButton(start);
		
		JButton stop = new JButton("Stop");
		stop.addActionListener(new StopChicken(c));
		_barnyard.addButton(stop);
	}

}
