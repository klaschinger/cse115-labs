package code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import example1.Chicken;

public class StartChicken implements ActionListener {

	Chicken _chicken;
	
	public StartChicken(Chicken c){
		_chicken = c;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		_chicken.start();
	}

}
