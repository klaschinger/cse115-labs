package code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import example1.Chicken;

public class StopChicken implements ActionListener {

	Chicken _chicken;
	
	public StopChicken(Chicken c){
		_chicken = c;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		_chicken.stop();
	}

}
