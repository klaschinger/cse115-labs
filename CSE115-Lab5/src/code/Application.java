package code;

import javax.swing.JButton;

import example1.BarnYard;

public class Application implements Runnable {

	public Application() {
		// do nothing here
	}
	
	@Override
	public void run() {
		// do your initialization here
		
		BarnYard by = new BarnYard();
		JButton button =  new JButton("Add Chicken");
		
		button.addActionListener(new AddChickenEvent(by));
		
		by.addButton(button);
	}


}
