package code.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import code.fileIO.FileIO;
import code.model.KeyBricksModel;

public class SaveHandler implements ActionListener {

	private KeyBricksModel _model;
	
	public SaveHandler(KeyBricksModel model) {
		_model = model;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		FileIO.writeStringToFile(System.getProperty("user.name")+".kbr", _model.toString());
		System.out.println("File saved in: " + System.getProperty("user.name") + ".kbr");
		
	}

}
