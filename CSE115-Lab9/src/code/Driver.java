package code;

import javax.swing.SwingUtilities;

import code.fileIO.FileIO;
import code.gui.KeyBricksGUI;
import code.model.KeyBricksModel;

public class Driver {
	public static void main(String[] args) {
		
		KeyBricksModel model = null;
		if(args.length == 0){
			model = new KeyBricksModel();
		} else {
			String contents = FileIO.readFileToString(args[0]);
			
			if (contents.equals("")){
				model = new KeyBricksModel();
			} else {
				model = new KeyBricksModel(contents);
			}
		}
		
		SwingUtilities.invokeLater(new KeyBricksGUI(model));
	}
}
