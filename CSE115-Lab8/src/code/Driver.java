package code;

import javax.swing.SwingUtilities;

import com.laschinger.keybricks.KeyBricks;

public class Driver {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new KeyBricks());
	}
}
