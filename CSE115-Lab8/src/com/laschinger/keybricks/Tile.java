package com.laschinger.keybricks;

import java.awt.Color;
import java.util.HashSet;

public class Tile {
	
	private int _posX;
	private int _posY;
	private char _letter;
	private Color _color;
	
	public Tile(int posX, int posY, Color color){
		_posX = posX;
		_posY = posY;
		_color = color;
		_letter = '!';
	}
	
	public HashSet<Tile> removeSurrounding(HashSet<Tile> set, HashSet<Tile> remove){
		
		remove.add(this);
		
		for(Tile tile: set){
			if (tile.getXPos() == getXPos()+1 && tile.getYPos() == getYPos() && tile.getColor().equals(this.getColor()) && !remove.contains(tile)){
				remove.addAll(tile.removeSurrounding(set, remove));
			} else if (tile.getXPos() == getXPos()-1 && tile.getYPos() == getYPos() && tile.getColor().equals(this.getColor()) && !remove.contains(tile)){
				remove.addAll(tile.removeSurrounding(set, remove));
			} else if (tile.getXPos() == getXPos() && tile.getYPos() == getYPos()+1 && tile.getColor().equals(this.getColor()) && !remove.contains(tile)){
				remove.addAll(tile.removeSurrounding(set, remove));
			} else if (tile.getXPos() == getXPos() && tile.getYPos() == getYPos()-1 && tile.getColor().equals(this.getColor()) && !remove.contains(tile)){
				remove.addAll(tile.removeSurrounding(set, remove));
			}
		}
		
		return remove;
	}
	
	public boolean hasPos(int x, int y){
		if (this.getXPos() == x && this.getYPos() == y){
			return true;
		}
		return false;
	}
	
	public int getXPos(){
		return _posX;
	}
	
	public void setXPos(int x){
		_posX = x;
	}
	
	public int getYPos(){
		return _posY;
	}
	
	public void setYPos(int y){
		_posY = y;
	}
	
	public Color getColor(){
		return _color;
	}
	
	public char getLetter(){
		return _letter;
	}
	
	public void setLetter(char l){
		_letter = l;
	}

}
