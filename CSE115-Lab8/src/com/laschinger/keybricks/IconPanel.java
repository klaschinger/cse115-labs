package com.laschinger.keybricks;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class IconPanel extends JPanel {
	
	private String _imageLocation;
	
	public IconPanel(String loc){
		_imageLocation = loc;
	}
	
	public void paintCompontent(Graphics g){
		super.paintComponent(g);
		
		Image image;
		
		try {
			image = ImageIO.read(IconPanel.class.getClassLoader().getResource(_imageLocation));
			int x = getWidth()/2 - image.getWidth(null)/2;
			int y = getHeight()/2 - image.getHeight(null)/2;
			g.drawImage(image, x, y, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
