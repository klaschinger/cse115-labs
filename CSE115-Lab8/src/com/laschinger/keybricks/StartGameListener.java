package com.laschinger.keybricks;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.HashSet;

import javax.swing.JButton;

public class StartGameListener implements ActionListener {

	private GamePanel _gamePanel;
	private HashSet<JButton> _buttonSet;
	private Image _icon;
	
	public StartGameListener(GamePanel panel, HashSet<JButton> buttonSet, Image icon){
		_gamePanel = panel;
		_buttonSet = buttonSet;
		_icon = icon;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		for (JButton button: _buttonSet){
			button.setEnabled(false);
		}
		
		Game game = new Game(KeyBricks.getSize()[0], KeyBricks.getSize()[1], _gamePanel, _buttonSet, _icon);
		for (KeyListener l: _gamePanel.getKeyListeners()){
			_gamePanel.removeKeyListener(l);
		}
		
		_gamePanel.addKeyListener(game);
		_gamePanel.setGameTiles(game.getTiles());
		_gamePanel.paintComponents(_gamePanel.getGraphics());
		_gamePanel.requestFocus();

	}

}
