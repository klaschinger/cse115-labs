package com.laschinger.keybricks;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SliderListener implements ChangeListener {

	@Override
	public void stateChanged(ChangeEvent e) {
		
		if (e.getSource() instanceof JSlider){
			JSlider slider = (JSlider) e.getSource();
			KeyBricks.setColors(slider.getValue());
		}
	}

}
