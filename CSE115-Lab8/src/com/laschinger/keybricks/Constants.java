package com.laschinger.keybricks;

public class Constants {
	
	public static final int NUMBER_OF_LETTERS = 4;
	public static final int GUI_BUFFER_SPACE = 10;
	public static final int[] EASY = {1,4};
	public static final int[] MEDIUM = {4,4};
	public static final int[] HARD = {8,8};
	public static final int INITIAL_COLOR_AMOUNT = 4;
	public static final int WINDOW_MIN_X = 800;
	public static final int WINDOW_MIN_Y = 600;
	public static final int CLOCK_TIME = 2000;
	public static final int FONT_SMALL = 10;
	public static final int FONT_MEDIUM = 7;
	public static final int FONT_LARGE = 5;

}
