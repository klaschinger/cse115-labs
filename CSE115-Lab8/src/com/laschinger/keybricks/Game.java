package com.laschinger.keybricks;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Game implements KeyListener {

	private HashSet<Tile> _gameTiles = new HashSet<Tile>();
	private GamePanel  _panel;
	private HashSet<JButton> _buttonSet;
	private Image _icon;
	private HashSet<Character> _unvalidLetters;
	
	public Game(int horz, int vert, GamePanel panel, HashSet<JButton> buttonSet, Image icon){
		for (int i = 0; i < horz; i++){
			for (int j = 0; j < vert; j++){
				_gameTiles.add(new Tile(i, j, getRandomColor()));
			}
		}
		
		_panel = panel;
		_buttonSet = buttonSet;
		_icon = icon;
		
		_unvalidLetters = new HashSet<Character>();
		
		assignRandomLetter();
	}
	
	private void assignRandomLetter(){
		while (_unvalidLetters.size() < Constants.NUMBER_OF_LETTERS) {
			Random rand = new Random();
			char[] letters = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
			
			int x = rand.nextInt(KeyBricks.getSize()[0]);
			int y = rand.nextInt(KeyBricks.getSize()[1]);
			char letter = letters[rand.nextInt(26)];
			
			for (Tile tile: _gameTiles){
				if (tile.hasPos(x, y) && tile.getLetter()=='!' && !_unvalidLetters.contains((Character) letter)){
					tile.setLetter(letter);
					_unvalidLetters.add((Character) letter);
				}
			}
		}	
	}

	private Color getRandomColor(){
		Random rand = new Random();
		int i = rand.nextInt(KeyBricks.getColors());
		
		switch(i){
			case 0 :
				return Color.RED;
			case 1 :
				return Color.BLUE;
			case 2 :
				return Color.GREEN;
			case 3 :
				return Color.YELLOW;
			case 4 :
				return Color.ORANGE;
			case 5 :
				return Color.MAGENTA;
			case 6 :
				return Color.PINK;
			case 7 :
				return Color.CYAN;
			case 8 :
				return Color.GRAY;
			case 9 :
				return Color.DARK_GRAY;
		}
		return null;
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {

	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		Tile removeTile = null;
		HashSet<Tile> removeTiles = new HashSet<Tile>();
		
		for (Tile tile: _gameTiles ) {
			if (e.getKeyChar() != '!' && Character.toLowerCase(e.getKeyChar()) == Character.toLowerCase(tile.getLetter())){
				removeTile = tile;
			}
		}
		
		if(removeTile != null){
			removeTiles = removeTile.removeSurrounding(_gameTiles, removeTiles);
		}
		
		for (Tile tile: removeTiles){
			if (tile.getLetter() != '!') {
				_unvalidLetters.remove((Character) tile.getLetter());
			}
			_gameTiles.remove(tile);
		}
		
		if (_gameTiles.size() >= 4){
			assignRandomLetter();
		}
		
		_panel.updateGraphics(_gameTiles);
		
		if (_gameTiles.size() == 0){
			showWinScreen();
		}
	}

	public HashSet<Tile> getTiles() {
		return _gameTiles;
	}
	
	public void showWinScreen(){
		
		JFrame frame = new JFrame("Winner");
		JPanel panel = new JPanel();
		JLabel header = new JLabel("Congratulations!!!");
		JLabel text = new JLabel("You have won the game!!!");
		
		frame.setAlwaysOnTop(true);
		frame.setMinimumSize(new Dimension(400, 150));
		frame.setResizable(false);
		frame.addWindowListener(new WinWindowEvent(_buttonSet));
		frame.add(panel);
		frame.setIconImage(_icon);
		frame.setLocation((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2 - 125), (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()/2 - 150));
		
		panel.setLayout(null);
		panel.add(header);
		panel.add(text);
		
		header.setFont(new Font("Times New Roman",Font.BOLD, 48));
		header.setSize(header.getPreferredSize());
		header.setLocation(frame.getWidth()/2 - header.getWidth()/2, 10);
		
		text.setFont(new Font("Times New Roman",Font.BOLD, 20));
		text.setSize(text.getPreferredSize());
		text.setLocation(frame.getWidth()/2 - text.getWidth()/2, 80);
		
		frame.pack();
		frame.setVisible(true);
	}

}
