package com.laschinger.keybricks;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GraphicsClock implements ActionListener {
	
	private GamePanel _gamePanel;

	public GraphicsClock(GamePanel gamePanel) {
		_gamePanel = gamePanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		_gamePanel.paintComponents(_gamePanel.getGraphics());
	}

}
