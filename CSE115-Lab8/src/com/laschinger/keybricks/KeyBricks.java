package com.laschinger.keybricks;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class KeyBricks implements Runnable, ComponentListener{
	
	private static BufferedImage _gameIcon;
	private static BufferedImage _settingsIcon;
	private static BufferedImage _logo;
	private static HashMap<Color,BufferedImage> _tileIcons = new HashMap<Color, BufferedImage>();
	private static BufferedImage _background;
	private static BufferedImage _letters;
	private static int[] _size = Constants.EASY;
	private static int _colorAmount = Constants.INITIAL_COLOR_AMOUNT;
	
	private JFrame _frame;
	private JPanel _mainPanel;
	private JPanel _controlPanel;
	private GamePanel _gamePanel;
	private IconPanel _iconPanel;
	private JLabel _newGameText;
	private JButton _newGame;
	private JLabel _scoreText;
	private JLabel _score;
	private JLabel _singlesText;
	private JPanel _singles;
	private JLabel _settingsText;
	private JButton _settings;
	
	private int _scoreCount;
	private HashSet<JButton> _buttonSet;

	@Override
	public void run() {
		LoadImages();
		
		_frame = new JFrame("KeyBricks");
		_mainPanel = new JPanel();
		_controlPanel = new JPanel();
		_gamePanel = new GamePanel(_tileIcons, _background, _letters);
		_iconPanel = new IconPanel("logo.png");
		_newGameText = new JLabel("New Game");
		_newGame = new JButton("Start");
		_scoreText = new JLabel("Score");
		_score = new JLabel("0");
		_singlesText = new JLabel("Single Blocks");
		_singles = new JPanel();
		_settingsText = new JLabel("Settings");
		_settings = new JButton();
		
		_scoreCount = 0;
		_buttonSet = new HashSet<JButton>();
		_buttonSet.add(_newGame);
		_buttonSet.add(_settings);
		
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_frame.setMinimumSize(new Dimension(Constants.WINDOW_MIN_X,Constants.WINDOW_MIN_Y));
		_frame.setIconImage(_gameIcon);
		_frame.addComponentListener(this);
		_frame.add(_mainPanel);
		
		_mainPanel.setLayout(null);
		_mainPanel.add(_controlPanel);
		_mainPanel.add(_gamePanel);
		
		_controlPanel.setLayout(null);
		_controlPanel.setLocation(0,0);
		_controlPanel.add(_newGameText);
		_controlPanel.add(_newGame);
		_controlPanel.add(_scoreText);
		_controlPanel.add(_score);
		_controlPanel.add(_singlesText);
		_controlPanel.add(_singles);
		_controlPanel.add(_settingsText);
		_controlPanel.add(_settings);
		
		_newGame.addActionListener(new StartGameListener(_gamePanel, _buttonSet, _gameIcon));
		
		_settings.addActionListener(new SettingsListener(_gameIcon));
		
		_frame.pack();
		_frame.setVisible(true);
		
		Timer timer = new Timer(Constants.CLOCK_TIME, new GraphicsClock(_gamePanel));
		timer.start();
	}
	
	private static void LoadImages(){
		try{
			_gameIcon = ImageIO.read(KeyBricks.class.getClassLoader().getResource("icon.png"));
			_settingsIcon = ImageIO.read(KeyBricks.class.getClassLoader().getResource("settings_icon.png"));
			//_logo = ImageIO.read(KeyBricks.class.getClassLoader().getResource("logo.png"));
			_tileIcons.put(Color.RED, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_red.png")));
			_tileIcons.put(Color.BLUE, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_blue.png")));
			_tileIcons.put(Color.GREEN, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_green.png")));
			_tileIcons.put(Color.YELLOW, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_yellow.png")));
			_tileIcons.put(Color.ORANGE, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_orange.png")));
			_tileIcons.put(Color.MAGENTA, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_magenta.png")));
			_tileIcons.put(Color.PINK, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_pink.png")));
			_tileIcons.put(Color.CYAN, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_cyan.png")));
			_tileIcons.put(Color.GRAY, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_gray.png")));
			_tileIcons.put(Color.DARK_GRAY, ImageIO.read(KeyBricks.class.getClassLoader().getResource("tile_dark_gray.png")));
			_background = ImageIO.read(KeyBricks.class.getClassLoader().getResource("background.png"));
			_letters = ImageIO.read(KeyBricks.class.getClassLoader().getResource("letters.png"));
		} catch (IOException e){
			e.printStackTrace();
		} catch (NullPointerException e ) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
	
	public void UpdateScore(int i) {
		_scoreCount = _scoreCount + i;
		_score.setText(String.valueOf(_scoreCount));
	}
	
	//Getters and Setters
	public static int[] getSize(){
		return _size;
	}
	
	public static void setSize(int[] i){
		_size = i;
	}

	public static int getColors() {
		return _colorAmount;
	}
	
	public static void setColors(int colors){
		_colorAmount = colors;
	}

	//Resize Actions
	@Override
	public void componentHidden(ComponentEvent arg0) {
		
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
		
	}

	@Override
	public void componentResized(ComponentEvent e) {
		_controlPanel.setSize(_frame.getWidth()/4, _frame.getHeight());

		_gamePanel.setSize((_frame.getWidth()/4)*3 - 20, _frame.getHeight()-20);
		_gamePanel.setLocation(_frame.getWidth()/4, 0);

		//_iconPanel.setSize(_controlPanel.getWidth()-20, _logo.getHeight());
		_iconPanel.setLocation(10, 10);
		_iconPanel.paintComponents(_iconPanel.getGraphics());
		
		_newGame.setFont(new Font("Times New Roman", Font.PLAIN, _controlPanel.getWidth()/Constants.FONT_SMALL));
		_newGame.setSize(_newGame.getPreferredSize());
		_newGame.setLocation(_controlPanel.getWidth()/2 - _newGame.getWidth()/2, _iconPanel.getHeight() + 2*Constants.GUI_BUFFER_SPACE);
		
		_settings.setIcon(new ImageIcon(_settingsIcon.getScaledInstance(_newGame.getHeight(), _newGame.getHeight(), Image.SCALE_SMOOTH)));
		_settings.setSize(_settings.getPreferredSize());
		_settings.setLocation(_controlPanel.getWidth()/2 - _settings.getWidth()/2, _controlPanel.getHeight() - _settings.getHeight()*2 - Constants.GUI_BUFFER_SPACE);
		
		_settingsText.setFont(new Font("Times New Roman", Font.PLAIN, _controlPanel.getWidth()/Constants.FONT_MEDIUM));
		_settingsText.setSize(_settingsText.getPreferredSize());
		_settingsText.setLocation(_controlPanel.getWidth()/2 - _settingsText.getWidth()/2, _controlPanel.getHeight() - _settingsText.getHeight() - _settings.getHeight()*2 - Constants.GUI_BUFFER_SPACE*2);
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
		
	}

}
