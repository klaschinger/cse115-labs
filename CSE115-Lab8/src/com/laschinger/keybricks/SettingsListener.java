package com.laschinger.keybricks;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;

public class SettingsListener implements ActionListener {

	private Image _icon;
	
	public SettingsListener(Image icon){
		_icon = icon;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		JButton button = (JButton) e.getSource();
		button.setEnabled(false);
		
		JFrame frame = new JFrame("Game Settings");
		JPanel panel = new JPanel();
		JLabel settingsText = new JLabel("Settings");
		JLabel colorAmount = new JLabel("Amount of Colors");
		JSlider slider = new JSlider(3, 10);
		JLabel difficultyText = new JLabel("Difficulty");
		JRadioButton easy = new JRadioButton("Easy");
		JRadioButton medium = new JRadioButton("Medium",true);
		JRadioButton hard = new JRadioButton("Hard");
		ButtonGroup difficulty = new ButtonGroup();
		difficulty.add(easy);
		difficulty.add(medium);
		difficulty.add(hard);
		
		frame.setAlwaysOnTop(true);
		frame.setMinimumSize(new Dimension(300, 250));
		frame.setResizable(false);
		frame.addWindowListener(new SettingsWindowEvent(button));
		frame.add(panel);
		frame.setIconImage(_icon);
		frame.setLocation((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2 - 125), (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()/2 - 150));
		
		panel.setLayout(null);
		panel.add(settingsText);
		panel.add(colorAmount);
		panel.add(slider);
		panel.add(difficultyText);
		panel.add(easy);
		panel.add(medium);
		panel.add(hard);
		
		settingsText.setFont(new Font("Times New Roman",Font.BOLD, 48));
		settingsText.setSize(settingsText.getPreferredSize());
		settingsText.setLocation(frame.getWidth()/2 - settingsText.getWidth()/2, 10);
		
		colorAmount.setFont(new Font("Times New Roman", Font.PLAIN, 30));
		colorAmount.setSize(colorAmount.getPreferredSize());
		colorAmount.setLocation(frame.getWidth()/2 - colorAmount.getWidth()/2, 80);
		
		slider.setValue(KeyBricks.getColors());
		slider.setMajorTickSpacing(1);
		slider.setSnapToTicks(true);
		slider.setPaintTicks(true);
		Dictionary<Integer, Component> numMap = new Hashtable<Integer, Component>();
		numMap.put(3, new JLabel("3"));
		numMap.put(4, new JLabel("4"));
		numMap.put(5, new JLabel("5"));
		numMap.put(6, new JLabel("6"));
		numMap.put(7, new JLabel("7"));
		numMap.put(8, new JLabel("8"));
		numMap.put(9, new JLabel("9"));
		numMap.put(10, new JLabel("10"));
		slider.setLabelTable(numMap);
		slider.setSize(slider.getPreferredSize());
		slider.setLocation(frame.getWidth()/2 - slider.getWidth()/2, 120);
		slider.addChangeListener(new SliderListener());
		
		difficultyText.setFont(new Font("Times New Roman",Font.PLAIN, 30));
		difficultyText.setSize(difficultyText.getPreferredSize());
		difficultyText.setLocation(frame.getWidth()/2 - difficultyText.getWidth()/2, 150);
		
		easy.setFont(new Font("Times New Roman", Font.BOLD, 16));
		easy.setSize(easy.getPreferredSize());
		easy.setLocation(frame.getWidth()/4 - easy.getWidth()/2, 190);
		easy.addActionListener(new ButtonChangeListener(difficulty));
		
		medium.setFont(new Font("Times New Roman", Font.BOLD, 16));
		medium.setSize(medium.getPreferredSize());
		medium.setLocation(frame.getWidth()/2 - medium.getWidth()/2, 190);
		medium.addActionListener(new ButtonChangeListener(difficulty));
		
		hard.setFont(new Font("Times New Roman", Font.BOLD, 16));
		hard.setSize(hard.getPreferredSize());
		hard.setLocation((frame.getWidth()/4)*3 - hard.getWidth()/2, 190);
		hard.addActionListener(new ButtonChangeListener(difficulty));
		
		frame.pack();
		frame.setVisible(true);
	}

}
