package com.laschinger.keybricks;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

public class ButtonChangeListener implements ActionListener {

	ButtonGroup _group;
	
	public ButtonChangeListener(ButtonGroup g){
		_group = g;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JRadioButton){
			JRadioButton button = (JRadioButton) e.getSource();
			int[] i = {0,0};
			
			_group.clearSelection();
			
			if(button.getText().equals("Easy")){
				i[0] = Constants.EASY[0];
				i[1] = Constants.EASY[1];;
			} else if (button.getText().equals("Medium")) {
				i[0] = Constants.MEDIUM[0];
				i[1] = Constants.MEDIUM[1];
			} else if (button.getText().equals("Hard")) {
				i[0] = Constants.HARD[0];
				i[1] = Constants.HARD[1];
			}
			
			KeyBricks.setSize(i);
			button.setSelected(true);
		}

	}

}
